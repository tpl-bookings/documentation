# Documentation for TPL Bookings group!

We can use this repo to store Markdown or other source files, diagrams, Sketch or Photoshop files. We can also use the attached Wiki to write more comfortably. And we can make issues here for what needs to be documented.